package com.techu.apirest.service;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    //con el autowired no es necesrio hacer un new. Cuando lo usemos, spring lo crea solito
    @Autowired
    ProductoRepository productoRepository;

    //READ
    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }

    //READ BY ID
    //optional --> si una clase no devuelve el tipo de dato que espera el método, que lo trate igualmente...
    // es porque el findid igual no encuentra producto con ese ID, ojo...
    public Optional<ProductoModel> findById(String id){
        return productoRepository.findById(id);
    }



    //CREATE
    public ProductoModel save(ProductoModel producto){
        return productoRepository.save(producto);
    }

    //DELETE
    public boolean delete(ProductoModel producto){
        try {
            productoRepository.delete(producto);
            return true;
        }catch(Exception e){
            return false;
        }
        //habría que controlar bien esto, ya que siempre da true, porque el try solo casca si hay algún
        //problema con el delete. Si intentamos borrar uno no existente, no pasa nada, no casca, así que da true igual
    }

    //DELETEBYID
    public void deleteById(String id){
        productoRepository.deleteById(id);
    }

    //existsby ID
    public boolean existsById(String id){
        return(productoRepository.existsById(id));
    }

}
