package com.techu.apirest.service;

import com.techu.apirest.model.ClientModel;
import com.techu.apirest.model.VehicleModel;
import com.techu.apirest.repository.ClientRepository;
import com.techu.apirest.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleService {

    //con el autowired no es necesrio hacer un new. Cuando lo usemos, spring lo crea solito
    @Autowired
    VehicleRepository vehicleRepository;

    //READ
    public List<VehicleModel> findAll(){
        return vehicleRepository.findAll();
    }

    //READ BY ID
    //optional --> si una clase no devuelve el tipo de dato que espera el método, que lo trate igualmente...
    // es porque el findid igual no encuentra producto con ese ID, ojo...
    public Optional<VehicleModel> findById(String id){
        return vehicleRepository.findById(id);
    }


    //CREATE
    public VehicleModel save(VehicleModel vehicle){
        return vehicleRepository.save(vehicle);
    }

    //DELETE
    public boolean delete(VehicleModel vehicle){
        try {
            vehicleRepository.delete(vehicle);
            return true;
        }catch(Exception e){
            return false;
        }
        //habría que controlar bien esto, ya que siempre da true, porque el try solo casca si hay algún
        //problema con el delete. Si intentamos borrar uno no existente, no pasa nada, no casca, así que da true igual
    }

    //DELETEBYID
    public void deleteById(String id){
        vehicleRepository.deleteById(id);
    }

    //existsby ID
    public boolean existsById(String id){
        return(vehicleRepository.existsById(id));
    }

}
