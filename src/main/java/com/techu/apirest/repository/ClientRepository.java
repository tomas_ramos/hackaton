package com.techu.apirest.repository;


import com.techu.apirest.model.ClientModel;
import com.techu.apirest.model.ProductoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends MongoRepository<ClientModel, String> {

}