package com.techu.apirest.model;


import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "clientes") //para que productoModel sea una colección de la BBDD
public class ClientModel {
    @Id
    @NotNull
    private String idClient;
    @NotNull
    private String name;
    private String idVehicle;
   // private VehicleModel vehicleModel;
    private Integer phone;

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdVehicle() {
        return idVehicle;
    }

    public void setIdVehicle(String idVehicle) {
        this.idVehicle = idVehicle;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public ClientModel(@NotNull String idClient, @NotNull String name, String idVehicle, Integer phone) {
        this.idClient = idClient;
        this.name = name;
        this.idVehicle = idVehicle;
        this.phone = phone;
    }

    public ClientModel(){

    }
}
